package coordinates;

public class Coordinates {

    private double latitude;
    private double longitude;
    public Coordinates(double latitud, double longitud){
        this.latitude = latitud;
        this.longitude = longitud;
    }

    public double getLatitude(){
        return  this.latitude;
    }

    public double getLongitude(){
        return this.longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Coordinates) {
            Coordinates coord = (Coordinates) o;
            return (this.getLatitude() == coord.getLatitude()) && (this.getLongitude() == coord.getLongitude());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Coordenates{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
