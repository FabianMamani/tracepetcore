package tracePetProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TracePetProperties {
    private static TracePetProperties instance = null;
    private Properties properties;

    private TracePetProperties(String pathProperties) {
        properties = new Properties();
        try {
            properties.load(new FileInputStream(new File(pathProperties)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static TracePetProperties getInstance(String pathProperties){
        if(instance == null){
            synchronized (TracePetProperties.class){
                if(instance == null)
                    instance = new TracePetProperties(pathProperties);
            }
        }
        return instance;
    }

    public String get(String key){
        return properties.getProperty(key);
    }
}
