package reader;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.util.Map;

public class JSONReader {

    private Gson gson;

    public JSONReader(){
        gson = new Gson();
    }

    public Map readFile(String archivo) throws FileNotFoundException {
        JsonReader getLocalJSONFile = new JsonReader(new FileReader(archivo));
        Map jsonLocations = gson.fromJson(getLocalJSONFile, Map.class);
        return jsonLocations;
    }
}