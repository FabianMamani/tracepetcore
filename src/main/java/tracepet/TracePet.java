package tracepet;

import breaker.NotifierCircuitBreaker;
import channelNotification.MainNotifier;
import coordinates.Coordinates;
import discovery.Notifier;
import observer.LocatorObservable;
import observer.LocatorObserver;
import pet.Pet;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class TracePet implements LocatorObservable {

    private MainNotifier notificator;
    Set<Notifier> notifierSet;
    private NotifierCircuitBreaker notifierCircuitBreaker;
    private Map<Pet, Coordinates> locations;
    private ArrayList<LocatorObserver> observers;
    private Coordinates location;
    public TracePet(Map <Pet, Coordinates> locations, Set<Notifier> notifiers){
        this.notifierSet = notifiers;
        this.observers = new ArrayList<LocatorObserver>();
        this.locations = locations;
        try {
            this.notificator = new MainNotifier(this.notifierSet);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        this.notifierCircuitBreaker = new NotifierCircuitBreaker();
    }
    /** Metodo para actualizar las coordenadas **/
    public void updateLocation(Pet pet, Coordinates location){
        if(!this.locations.containsKey(pet))
            throw new IllegalArgumentException("No se puede actualizar una mascota no registrada");

            this.locations.put(pet, location);
            this.location = locations.put(pet, location);
            notifyLocation();
    }
    public Coordinates locatePet(String petName){
        if (petName==null)
            throw new IllegalArgumentException("No se seleccionó una mascota valida para la búsqueda");
        Pet petFound = null;
        this.location = null;

        for (Pet pet: this.locations.keySet()) {
            if (pet.getName().equals(petName)) {
                petFound = pet;
                this.location = this.locations.get(pet);
            }
        }
        if (petFound == null)
            throw new NullPointerException("Mascota no registrada: " + petName);
        if (this.location == null)
            throw new NullPointerException("La mascota " + petName + " no pudo ser localizada");
        notifyLocation();
        return this.location;
    }

    public NotifierCircuitBreaker getNotifierCircuitBreaker(){
        return this.notifierCircuitBreaker;
    }

    public MainNotifier getNotificator(){
        return this.notificator;
    }

    @Override
    public void subscribe(LocatorObserver o) {
        this.observers.add(o);
    }

    @Override
    public void unsubscribe(LocatorObserver o) {
        this.observers.remove(o);
    }

    @Override
    public void notifyLocation() {
        for (LocatorObserver o: this.observers) {
            o.update(this.location);
        }
    }
}
