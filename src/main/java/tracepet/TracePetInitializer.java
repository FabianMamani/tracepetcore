package tracepet;

import com.google.gson.internal.LinkedTreeMap;
import coordinates.Coordinates;
import discovery.Notifier;
import discovery.NotifierDiscovery;
import pet.Pet;
import reader.JSONReader;
import tracePetProperties.TracePetProperties;

import java.io.FileNotFoundException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TracePetInitializer {

    private static TracePetProperties tracePetProperties = null;
    private static final NotifierDiscovery notifierFactory = new NotifierDiscovery();
    public static TracePet buildTracePet(String pathProperties) throws NoSuchFileException {
        tracePetProperties = TracePetProperties.getInstance(pathProperties);
        return new TracePet(loadLocations(),findNotifiers());
    }

    public static Map<Pet, Coordinates> loadLocations(){
        Map<Pet, Coordinates> locations = new HashMap<>();
        JSONReader reader = new JSONReader();
        try {
            Map jsonLocations = reader.readFile(tracePetProperties.get("path_jsonLocations"));
            ArrayList<LinkedTreeMap> pets = (ArrayList<LinkedTreeMap>)jsonLocations.get("mascotas");
            for (LinkedTreeMap jsonPet: pets) {
                Pet pet = new Pet((String)jsonPet.get("nombre"));
                LinkedTreeMap jsonCoordinate = (LinkedTreeMap) jsonPet.get("coordenada");
                Coordinates coordinate = null;
                if (jsonCoordinate != null) {
                    coordinate = new Coordinates(Double.parseDouble((String) jsonCoordinate.get("_latitud")),
                            Double.parseDouble((String) jsonCoordinate.get("_longitud")));
                }
                locations.put(pet,coordinate);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return locations;
    };

    public static Set<Notifier> findNotifiers() throws NoSuchFileException {
        return notifierFactory.discovery(tracePetProperties.get("path_notifiers"));
    }
}
