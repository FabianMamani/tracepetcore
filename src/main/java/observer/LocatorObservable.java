package observer;

public interface LocatorObservable {

    void subscribe(LocatorObserver o);
    void unsubscribe(LocatorObserver o);
    void notifyLocation();

}
