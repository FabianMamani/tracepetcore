package observer;

import coordinates.Coordinates;

public interface LocatorObserver {

    void update(Coordinates coordinates);

}
