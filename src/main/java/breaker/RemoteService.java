package breaker;

/**
 * La interfaz de servicio remoto, utilizada por {@link CircuitBreaker} para obtener la
 * respuesta del control remoto servicios.
 */
public interface RemoteService {

    //Obtenga la respuesta del servicio remoto.
    String call() throws RemoteServiceException;

}
