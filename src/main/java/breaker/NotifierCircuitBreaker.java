package breaker;

public class NotifierCircuitBreaker {

    // Estado final del circuit breaker que se modificara en el proceso para notificar la
    // completitud o no de las solicitudes
    private String finalStatus = "CLOSED";

    public void initCircuitBreaker(int timeout, long delay, int attempts){

        long serverStartTime = System.nanoTime();

        DelayedRemoteService delayedService = new DelayedRemoteService(serverStartTime, timeout);
        CircuitBreaker delayedServiceCircuitBreaker = new DefaultCircuitBreaker(delayedService,
                3000,1,2000 * 1000 * 1000);

        RemoteService quickService = new QuickRemoteService();
        CircuitBreaker quickServiceCircuitBreaker = new DefaultCircuitBreaker(quickService,
                3000,1,2000 * 1000 * 1000);

        // Crear un objeto de servicio de monitoreo que haga tanto local como llamadas remotas
        MonitoringService monitoringService = new MonitoringService(delayedServiceCircuitBreaker,
                quickServiceCircuitBreaker);

        // Obtener respuesta del recurso local
        System.out.println(monitoringService.localResourceResponse());

        // Obtenga la respuesta del servicio retrasado
        System.out.println(monitoringService.delayedServiceResponse());

        // Obtener el estado actual del circuit breaker de servicio retrasado después del cruce
        // límite de umbral de falla que está OPEN ahora
       delayedServiceCircuitBreaker.getState();

        // Mientras tanto, el servicio retrasado está inactivo, busque la respuesta del
        // servicio rapido saludable
        int i = 0;
        while(!delayedServiceCircuitBreaker.getState().equals("CLOSED") && i < attempts){
            // Wait for the delayed service to become responsive
            try {
                System.out.println("Waiting for delayed service to become responsive");
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(monitoringService.delayedServiceResponse());
            // Verifique el estado del circuit breaker retardado, debe estar HALF_OPEN
            System.out.println(delayedServiceCircuitBreaker.getState());
            i++;
        }

        // Obtenga la respuesta del servicio retrasado, que ya debería estar en buen estado
        System.out.println(monitoringService.delayedServiceResponse());
        // A medida que se obtiene una respuesta exitosa, debe CLOSED nuevamente.
        System.out.println(delayedServiceCircuitBreaker.getState());
        finalStatus = delayedServiceCircuitBreaker.getState();
    }
    // Se retorna el estado final del notifier circuit breaker
    public String getFinalStatus(){
        return finalStatus;
    }
}