package breaker;

public class DefaultCircuitBreaker implements CircuitBreaker {

    private final long timeout;
    private final long retryTimePeriod;
    private final RemoteService service;
    long lastFailureTime;
    private String lastFailureResponse;
    int failureCount;
    private final int failureThreshold;
    private State state;
    private final long futureTime = 1000 * 1000 * 1000 * 1000;

    /**
     * Constructor para crear una instancia de Circuit Breaker.
     *
     * @param timeout Tiempo de espera para la solicitud de API. No es necesario para este
     * ejemplo simple.
     * @param failureThreshold Número de fallas que recibimos del servicio dependiente
     * antes de cambiar
     * Estado para 'OPEN'
     * @param retryTimePeriod Período de tiempo después del cual se realiza una nueva solicitud al
     * servicio remoto para comprobación del estado.
     */

    DefaultCircuitBreaker(RemoteService serviceToCall, long timeout, int failureThreshold,
                          long retryTimePeriod) {
        this.service = serviceToCall;
        // Empezamos en estado closed esperando que este bien.
        this.state = State.CLOSED;
        this.failureThreshold = failureThreshold;
        // Tiempo de espera para la solicitud de API.
        // Se usa para interrumpir las llamadas realizadas al recurso remoto si excede el límite
        this.timeout = timeout;
        this.retryTimePeriod = retryTimePeriod;
        //Una cantidad absurda de tiempo en el futuro que básicamente indica que la última falla
        // nunca sucedió.
        this.lastFailureTime = System.nanoTime() + futureTime;
        this.failureCount = 0;
    }

    // Restablecer a los valores predeterminados
    @Override
    public void recordSuccess() {
        this.failureCount = 0;
        this.lastFailureTime = System.nanoTime() + futureTime;
        this.state = State.CLOSED;
    }

    @Override
    public void recordFailure(String response) {
        failureCount = failureCount + 1;
        this.lastFailureTime = System.nanoTime();
        // Almacene en caché la respuesta de falla para regresar al estado OPEN
        this.lastFailureResponse = response;
    }

    // Evalúe el estado actual en función de failureThreshold, failureCount y lastFailureTime.
    protected void evaluateState() {
        if (failureCount >= failureThreshold) { //Entonces algo anda mal con el servicio remoto
            if ((System.nanoTime() - lastFailureTime) > retryTimePeriod) {
                //Hemos esperado lo suficiente y deberíamos intentar verificar si el
                // servicio está activo.
                state = State.HALF_OPEN;
            } else {
                //El servicio probablemente seguiría caído
                state = State.OPEN;
            }
        } else {
            //Anda bien
            state = State.CLOSED;
        }
    }

    @Override
    public String getState() {
        evaluateState();
        return state.name();
    }

    /**
     * Interrumpa el circuito de antemano si se sabe que el servicio está caído O conecte el circuito
     * manualmente si el servicio se pone en línea antes de lo esperado.
     *
     * @param state Estado en el que se encuentra el circuito
     */
    @Override
    public void setState(State state) {
        this.state = state;
        switch (state) {
            case OPEN:
                this.failureCount = failureThreshold;
                this.lastFailureTime = System.nanoTime();
                break;
            case HALF_OPEN:
                this.failureCount = failureThreshold;
                this.lastFailureTime = System.nanoTime() - retryTimePeriod;
                break;
            default:
                this.failureCount = 0;
        }
    }

    /**
     * Ejecuta llamada de servicio.
     *
     * Valor @return del recurso remoto, respuesta obsoleta o una excepción personalizada
     */
    @Override
    public String attemptRequest() throws RemoteServiceException {
        evaluateState();
        if (state == State.OPEN) {
            // devolver la respuesta almacenada en caché si el circuito está en estado OPEN
            return this.lastFailureResponse;
        } else {
            // Realice la solicitud API si el circuito no está OPEN
            try {
                //En una aplicación real, esto se ejecutaría en un hilo y el tiempo de espera
                //parámetro del disyuntor se utilizaría para saber si el servicio
                //está trabajando. Aquí, simulamos eso en función de la respuesta del servidor en sí.
                String response = service.call();
                // La API respondió bien. Restablezcamos.
                recordSuccess();
                return response;
            } catch (RemoteServiceException ex) {
                recordFailure(ex.getMessage());
                throw ex;
            }
        }
    }
}
