package breaker;

/**
 * Excepción lanzada cuando {@link RemoteService} no responde correctamente.
 */
public class RemoteServiceException extends Exception {

    public RemoteServiceException(String message) {
        super(message);
    }
}