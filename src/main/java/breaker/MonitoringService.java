package breaker;

public class MonitoringService {

    private final CircuitBreaker delayedService;

    private final CircuitBreaker quickService;

    public MonitoringService(CircuitBreaker delayedService, CircuitBreaker quickService) {
        this.delayedService = delayedService;
        this.quickService = quickService;
    }

    //Suposición: el servicio local no fallará, no es necesario envolverlo en una lógica de
    // circuit breaker
    public String localResourceResponse() {
        return "Local Service is working";
    }

    /**
     * Obtenga la respuesta del servicio retrasado (con algún tiempo de inicio simulado).
     *
     * String de respuesta @return
     */
    public String delayedServiceResponse() {
        try {
            return this.delayedService.attemptRequest();
        } catch (RemoteServiceException e) {
            return e.getMessage();
        }
    }

    /**
     * Obtiene la respuesta de un servicio saludable sin fallas.
     *
     * String de respuesta @return
     */
    public String quickServiceResponse() {
        try {
            return this.quickService.attemptRequest();
        } catch (RemoteServiceException e) {
            return e.getMessage();
        }
    }
}
