package breaker;

public interface CircuitBreaker {

    // Respuesta de éxito. Restablecer todos a los valores predeterminados.
    void recordSuccess();

    // Respuesta de falla. Manipule en consecuencia con la respuesta y cambie el estado si es necesario.
    void recordFailure(String response);

    // Obtener el estado actual del circuit breaker.
    String getState();

    // Establezca el estado específico manualmente.
    void setState(State state);

    // Intente obtener la respuesta del servicio remoto.
    String attemptRequest() throws RemoteServiceException;
}
