package breaker;

/**
 * Enumeración de los estados en los que podría estar el circuit breaker.
 * CLOSED -> OK
 * OPEN -> Falla
 * HALF_OPEN -> Reintentando
 */
public enum State {
    CLOSED, OPEN, HALF_OPEN
}