package breaker;

public class DelayedRemoteService implements RemoteService {

    private final long serverStartTime;
    private final int delay;

    /**
     * Constructor para crear una instancia de DelayedService, que está inactivo durante
     * los primeros segundos.
     *
     * @param delay el retraso después del cual el servicio se comportaría correctamente, en segundos
     */
    public DelayedRemoteService(long serverStartTime, int delay) {
        this.serverStartTime = serverStartTime;
        this.delay = delay;
    }

    public DelayedRemoteService() {
        this.serverStartTime = System.nanoTime();
        this.delay = 20;
    }

    /**
     * Responde en función del retraso, la hora actual y la hora de inicio del servidor si
     * el servicio está inactivo/funcionando.
     *
     * @return El estado del servicio
     */
    @Override
    public String call() throws RemoteServiceException {
        long currentTime = System.nanoTime();
        //Dado que currentTime y serverStartTime están ambos en nanosegundos, lo convertimos a
        //segundos buceando por 10e9 y asegurando la división de punto flotante multiplicándolo
        //con 1.0 primero. Luego verificamos si es mayor o menor que el retraso especificado y luego
        //enviar la respuesta
        if ((currentTime - serverStartTime) * 1.0 / (1000 * 1000 * 1000) < delay) {
            //Puede usar Thread.sleep() aquí para bloquear y simular un servidor colgado
            throw new RemoteServiceException("Delayed service is down");
        }
        return "Delayed service is working";
    }
}
