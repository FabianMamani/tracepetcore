package breaker;

    /**
     * Un servicio remoto de respuesta rápida, que responde saludablemente sin ningún retraso o falla.
     */
    public class QuickRemoteService implements RemoteService {

        @Override
        public String call() throws RemoteServiceException {
            return "Quick Service is working";
        }
}
