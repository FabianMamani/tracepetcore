package channelNotification;

import discovery.Notifier;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainNotifier {
    private Notifier notifier;
    private String channel;
    private Map<String, Notifier> channels;

    public MainNotifier(Set<Notifier> notifierSet) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        loadChannels(notifierSet);
    }

    public void setChannel(String channelInput){
        if(countChannels()==0)
            throw new RuntimeException("No hay servicio de notificaciones disponible");
        notifier = channels.get(channelInput);
    }

    public void addNotifier(Notifier notifier) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        if(this.channels == null)
            this.channels =new HashMap<>();
        this.channels.put(getNotifierName(notifier), notifier.getClass().getConstructor().newInstance());
    }

    public String sendAlert(){
        return notifier.send();
    }

    public String getChannel(){
        return channel;
    }

    public int countChannels(){
        return channels.size();
    }

    public Collection getChannels(){
        return this.channels.keySet();
    }

    private void loadChannels(Set<Notifier> notifierSet) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        for (Notifier notifier: notifierSet) {
            addNotifier(notifier);
        }
    }

    private static String getNotifierName(Notifier notifier) {
        return notifier.getClass().getName().split("\\.")[notifier.getClass().getName().split("\\.").length-1];
    }
}
