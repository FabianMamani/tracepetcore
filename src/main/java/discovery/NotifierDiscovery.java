package discovery;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class NotifierDiscovery {

    private String packageName = "notifiers";

    public NotifierDiscovery() {}

    public Set<Notifier> discovery(String path) throws NoSuchFileException {
        Set<Notifier> result = new HashSet<>();
        validateDirectory(path);

        for (File f : new File(path).listFiles()){
            String className = f.getName();
            if ( !className.endsWith(".class")) continue;

            className = getFQDN(className, packageName);

            try {
                Class c = Class.forName(className);
                if (Notifier.class.isAssignableFrom(c)){
                    Notifier notifier = (Notifier) c.getDeclaredConstructor().newInstance();
                    result.add(notifier);
                }
            } catch (Exception e) { throw new RuntimeException(e); }
        }
        return result;
    }

    /**
     *
     * @param resourceName Recurso a obtener el Fully Qualified Domain Name
     * @return
     */
    private static String getFQDN(String resourceName, String packageName) {
        String fqdn = resourceName.substring(0, resourceName.lastIndexOf('.'));
        if (!packageName.equals("") && !packageName.isEmpty())
            fqdn = packageName + "." + fqdn;
        return fqdn;
    }

    private void validateDirectory(String path) throws NoSuchFileException {
        if(!Files.isDirectory(Paths.get(path)))
            throw new NoSuchFileException("El directorio no existe");
    }

    public Notifier getImpNotifier(Set<Notifier> notifierImpls, String implName) throws ClassNotFoundException {
         Notifier implementation = null;

         for (Notifier notifierImpl: notifierImpls) {
            if(notifierImpl.getClass().getName().equals(implName)){
                implementation = notifierImpl;
                break;
            }
         }
         if(implementation==null)
             throw new ClassNotFoundException("");
         return implementation;
    }
}
