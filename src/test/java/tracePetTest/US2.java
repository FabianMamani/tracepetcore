package tracePetTest;

import discovery.Notifier;
import discovery.NotifierDiscovery;
import org.junit.*;
import tracePetProperties.TracePetProperties;

import java.nio.file.NoSuchFileException;
import java.util.HashSet;
import java.util.Set;

public class US2 {

    private NotifierDiscovery plugin;
    private TracePetProperties tracePetProperties;
    private String pathProperties = "resources/config-dev.properties";

    @Before
    public void setUp(){
        plugin = new NotifierDiscovery();
        tracePetProperties = TracePetProperties.getInstance(pathProperties);
    }

    /**
     * Escenario 1
     * Carpeta vacia
     * */
    @Test
    public void CA1() throws NoSuchFileException {
        String dir = tracePetProperties.get("path_notifiers_empty");
        Assert.assertEquals(plugin.discovery(dir).size(),0);
    }

    /**
     * Escenario 2
     * Carpeta con implementaciones de Notifier
     * LocationNotifier
     */
    @Test
    public void CA2() throws NoSuchFileException, ClassNotFoundException {
        //Test donde se encuentra la implementacion del Plugin
        String dir = tracePetProperties.get("path_notifiers_single");
        Assert.assertEquals(asString(plugin.discovery(dir)), "[notifiers.Telegram]");
    }

    @Test
    public void CA3() throws NoSuchFileException, ClassNotFoundException {
        //Test donde ae encuentra mas de una implementacion del Plugin
        String dir = tracePetProperties.get("path_notifiers_many");
        Assert.assertEquals("[notifiers.Telegram, notifiers.Whatsapp]", asString(plugin.discovery(dir)));
    }

    /**
     * Escenario 3
     * Busqueda de implementaciones en una carpeta
     * que no tiene archivos de tipo .class
     */
    @Test
    public void CA4() throws NoSuchFileException {
        String dir = tracePetProperties.get("path_notifiers_notClasses");
        Assert.assertEquals(plugin.discovery(dir).size(),0);
    }

    /**
     * Escenario 4
     * Directorio que no existe
     */
    @Test(expected = NoSuchFileException.class)
    public void CA5() throws NoSuchFileException {
        //Test donde no se ingresa un directorio valido
        String dir = tracePetProperties.get("path_notifiers_notExists");
        Set results = plugin.discovery(dir);
    }

    private static String asString(Set<Notifier> results) {
        Set<String> classNames = new HashSet<>();
        for (Notifier notifier: results) {
            classNames.add(notifier.getClass().getName());
        }
        return classNames.toString();
    }
}
