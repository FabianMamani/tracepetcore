package tracePetTest;

import coordinates.Coordinates;
import pet.Pet;
import tracepet.TracePetInitializer;
import tracepet.TracePet;
import org.junit.*;

import java.nio.file.NoSuchFileException;

public class US1 {

    TracePet locator;

    @Before
    public void init() throws NoSuchFileException {
        String pathProperties = "resources/config-dev.properties";
        locator = new TracePetInitializer().buildTracePet(pathProperties);
    }

    @Test
    public void CA1(){
        //Test exitoso en donde se pasa una mascota valida y se recibe una coordenada valida
        //El tercer argumento es el margen de error y tiene que ser 0.0
        Coordinates location = locator.locatePet("Chocolate");
        Assert.assertEquals(2.0,location.getLongitude(),0.0);
        Assert.assertEquals(-1.0,location.getLatitude(),0.0);
    }

    @Test
    public void CA2(){
        //Test exitoso en donde se pasa una mascota y se actualiza la posicion
        Pet pet = new Pet("Chocolate");
        Coordinates location = new Coordinates(2,7);
        Coordinates prevLocation = locator.locatePet("Chocolate");
        locator.updateLocation(pet,location);
        Coordinates newLocation = locator.locatePet("Chocolate");
        Assert.assertEquals(newLocation,location);
    }

    @Test(expected = IllegalArgumentException.class)
    public void CA3(){
        //Test en donde se actualiza la posicion de una mascota no registrada
        Pet pet = new Pet("Mousse");
        Coordinates location = new Coordinates(2,7);
        locator.updateLocation(pet,location);
    }

    @Test(expected = NullPointerException.class)
    public void CA4(){
        //Test en donde se pasa una mascota no registrada
        Coordinates location = locator.locatePet("Mousse");
    }

    @Test(expected = IllegalArgumentException.class)
    public void CA5(){
        //Test en donde se pasa null por mascota
        Coordinates location = locator.locatePet(null);
    }

    @Test(expected = NullPointerException.class)
    public void CA6(){
        //Test en donde se actualiza la posicion de una mascota con una coordenada nula
        Pet pet = new Pet("Chocolate");
        locator.updateLocation(pet, null);
        Coordinates newLocation = locator.locatePet("Chocolate");
    }
}
