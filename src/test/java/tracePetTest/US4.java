package tracePetTest;

import breaker.NotifierCircuitBreaker;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class US4 {

    private NotifierCircuitBreaker notifierCircuitBreaker;

    @Before
    public void init(){
        notifierCircuitBreaker = new NotifierCircuitBreaker();
    }
    /**
     * Escenario 1
     * El servicio de mensajería funciona y hay internet
     */
    @Test
    public void CA1() {
        // Test donde el mensaje se envia al primer intento
        // No hay delay timeout en el servicio
        notifierCircuitBreaker.initCircuitBreaker(0,5000,3);
        String status = notifierCircuitBreaker.getFinalStatus();
        // Se espera el estado CLOSED
        Assert.assertEquals("CLOSED",status);
    }
    /**
     * Escenario 2
     * El servicio de mensajería no funciona o no hay internet por un tiempo menor al limite establecido
     */
    @Test
    public void CA2() {
        // Test donde el mensaje se envia en un reintento
        // Hay timeout de 5 seg pero se intenta 3 veces
        notifierCircuitBreaker.initCircuitBreaker(5,5000,3);
        String status = notifierCircuitBreaker.getFinalStatus();
        // Se espera el estado CLOSED
        Assert.assertEquals("CLOSED",status);
    }
    /**
     * Escenario 3
     * El servicio de mensajería no funciona o no hay internet por mas del tiempo limite establecido
     */
    @Test
    public void CA3() {
        // Test donde el mensaje no se puede enviar
        // Hay timeout de mas de 20 seg lo que sobrepasa el tiempo limite
        notifierCircuitBreaker.initCircuitBreaker(20,5000,3);
        String status = notifierCircuitBreaker.getFinalStatus();
        // Se espera el estado OPEN
        Assert.assertEquals("OPEN",status);
    }
}
