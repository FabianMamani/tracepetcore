package tracePetTest;

import channelNotification.MainNotifier;
import discovery.Notifier;
import notifiers.Telegram;
import notifiers.Whatsapp;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;

public class US5 {

    /**Escenario 1: No hay servicio de mensajería disponible.**/
    @Test(expected = java.lang.RuntimeException.class)
    public void CA1() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //No se podrá seleccionar ninguna mensajería.
        MainNotifier main = new MainNotifier(null);
        main.setChannel("Whatsapp");
    }

    /**Escenario 2: Solo hay un servicio de mensajería disponible.**/
    @Test
    public void CA2() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //Se tendrá un único servicio de mensajería.
        Notifier telegram = mock(Telegram.class);
        Set<Notifier> notifiers = new HashSet<>();
        notifiers.add(telegram);
        MainNotifier main = new MainNotifier(notifiers);
        Assert.assertEquals(1,main.countChannels());
    }

    /**Escenario 3: Agregar un nuevo notificador.**/
    @Test
    public void CA3() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Notifier whatsapp = mock(Whatsapp.class);
        Notifier telegram = mock(Telegram.class);
        Set<Notifier> notifiers = new HashSet<>();
        notifiers.add(telegram);
        MainNotifier main = new MainNotifier(notifiers);
        main.addNotifier(whatsapp);
        Assert.assertEquals(2,main.countChannels());
    }
}